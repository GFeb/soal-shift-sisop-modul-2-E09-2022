#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>
#include <stdbool.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>

int status;

void unzip() { //unzip file yang telah didownload
    	pid_t child_id;
    	child_id = fork();

    	if(child_id < 0) {
       		exit(EXIT_FAILURE);
    	}

    	if (child_id == 0) {
        	char *argv[] = {"unzip","-q","/home/firman/Downloads/drakor.zip","-x","*/*","-d","/home/firman/shift2/drakor",NULL}; 
        	execv("/bin/unzip", argv);
	} 
	while (wait(&status)>0);
}

void exefk(char command[], char *argv[]) { //agar dapat exe tanpa menunggu child selesai
        pid_t child_id;
        child_id = fork();

        if(child_id < 0) {
                exit(EXIT_FAILURE);
        }

        if(child_id == 0) {
                execv(command, argv);
        }
        while ((wait(&status)) > 0);
}

char* rmv(char* name){ //untuk menghilangkan format .png di nama
    int i, j, tmp;
    for(i = 0; name[i] != '\0'; i++);{
    	tmp = i - 3;
    }
    if(tmp < 1){
        return NULL;
    }
    char* del = (char*) malloc (tmp * sizeof(char));

    for(j = 0; j < tmp - 1; j++){
	del[j] = name[j];
    }
    del[j] = '\0';
    return del;
}

void mvFile() {
    DIR *drakor; //buka directory file yang sudah di unzip
    pid_t child_id;
    child_id = fork();
    
    if(child_id < 0) {
       	exit(EXIT_FAILURE);
    }

    if(child_id == 0) {
    	char src[101] = "/home/firman/shift2/drakor";
    	drakor = opendir(src); //open directorinya
	struct dirent *readDir; //membaca list folder dengan dirent
            
	    if(drakor != NULL) {
            	while((readDir = readdir(drakor))!= NULL){
                    if(readDir -> d_type == DT_REG){ //jika file bertipe reguler
                    	char *namaPoster = readDir -> d_name;
                    	char *tanpaPNG = rmv(namaPoster);
			char *tmp1, *tmp2, *tmp3, *tmp4;

                    	for(tmp1 = strtok_r(tanpaPNG,"_",&tmp3); tmp1 != NULL; tmp1 = strtok_r(NULL,"_",&tmp3)) { //strtok untuk memisahkan string dengan "_", in case memisahkan 2 poster
                           int strnum = 0;
			   char namaDrakor[100], tahun[100], genre[100];
                        
                           for(tmp2 = strtok_r(tmp1,";",&tmp4); tmp2 != NULL; tmp2 = strtok_r(NULL,";",&tmp4)) { //strtok untuk memisahkan string dengan ";", in case memisahkan nama, tahun, dan genre poster
                                if(strnum == 0) 
				    strcpy(namaDrakor, tmp2);//simpan nama
                                if(strnum == 1) 
				    strcpy(tahun, tmp2);//simpan tahun
                                if(strnum == 2) 
  				    strcpy(genre, tmp2);//simpan genre
                                strnum++;
                           }
			   char text[100], tmpsrc1[100], tmpsrc2[100], source[100]="/home/firman/shift2/drakor/";

                           strcpy(text, readDir -> d_name);
                           strcpy(tmpsrc1, source);

                           strcat(source, genre); //gabungkan untuk membuat nama folder berdasarkan genre
                           char *argv[] = {"mkdir","-p",source,NULL};//buat folder jika sebelumnya tidak ada
                           exefk("/bin/mkdir",argv);

                           char txtNama[100];
                           strcpy(txtNama, namaDrakor);//untuk output di .txt

                           strcat(namaDrakor,".png");//poster direname ulang sesuai soal
                           strcat(tmpsrc1, text);

                           char *cpy[] = {"cp","-r", tmpsrc1, source, NULL}; //copy filenya
                           exefk("/bin/cp",cpy);

                           char datatxt[100];
                           strcpy(datatxt, source);

                           strcpy(tmpsrc2, source);
                           strcat(tmpsrc2,"/");
                           strcat(tmpsrc2, text);
                           strcat(source,"/");
                           strcat(source, namaDrakor);

                           char *chgNama[] = {"mv",tmpsrc2,source,NULL}; //setelah copy, ubah namanya sesuai format
                       	   exefk("/bin/mv",chgNama);

                           strcat(datatxt,"/data.txt");
			   
			   char tmp[100];
                           strcpy(tmp,"kategori : ");
                           strcat(tmp, genre);
                           strcat(tmp,"\n\nnama : ");
                           strcat(tmp, txtNama);
                           strcat(tmp,"\nrilis: tahun ");
                           strcat(tahun,"\n\n");
                           strcat(tmp, tahun);

                           FILE *fdata;
                           fdata = fopen(datatxt,"a");
                           fputs(tmp, fdata);
                           fclose(fdata);
                        }
                    }
                    if(readDir -> d_type == DT_REG) {
                    	char source[101]="/home/firman/shift2/drakor/";
                    	strcat(source, readDir -> d_name);

                    	char *remove[] = {"rm","-rf",source,NULL}; //setelah file dipindahkan, hapus filen yg tersisa
                    	exefk("/bin/rm", remove);
                    }
               	}
            }
            closedir(drakor);
    }
    while ((wait(&status))>0);
}

int main() {
	pid_t child_id;
    	child_id = fork();
    	
	//jika child tidak berhasil
    	if(child_id < 0) {
        	exit(EXIT_FAILURE);
    	}
    	//jika berhasil
    	if(child_id == 0) {
		char *argv[] = {"mkdir","-p","/home/firman/shift2/drakor",NULL}; //buat folder
        	execv("/bin/mkdir", argv);
        }	
       	while((wait(&status))>0); //parent
   	unzip();
        mvFile();

	return 0;
}
