#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <string.h>
#include <dirent.h>

void listingFiles(){
    FILE *file = fopen("./home/dioz/modul2/air/list.txt", "a");
    if(file == NULL){
        printf("ERROR");
        exit(1);
    }

    DIR *dir;
    struct dirent *entry;
    int files = 0;

    dir = opendir("./home/dioz/modul2/air");

    if(dir == NULL){
        printf("ERROR");
        exit(1);
    }

    while(entry = readdir(dir)){
        char uid[100] = "";
        char permission[3] = "";
        struct stat fs;
        char format[360] = "";
        char path[40] = "./home/dioz/modul2/air/";

        cuserid(uid);
        strcat(path, entry->d_name);
        int r = stat(path, &fs);
        if( !(S_ISREG(fs.st_mode) && strcmp(entry->d_name, "list.txt") != 0)) continue;

        if( fs.st_mode & S_IRUSR )
        strcat(permission, "r");
        if( fs.st_mode & S_IWUSR )
        strcat(permission, "w");
        if( fs.st_mode & S_IXUSR )
        strcat(permission, "x");

        sprintf(format, "%s_%s_%s\n", uid, permission, entry->d_name);
        fprintf(file, format);
    }

    fclose(file);
}

int main() {
    pid_t child1_id;
    child1_id = fork();
    int status;

    if(child1_id < 0){
        exit(EXIT_FAILURE);
    }

    if(child1_id == 0){
        char *argv[] = {"mkdir", "-p", "home/dioz/modul2/darat", NULL};
        execv("/bin/mkdir", argv);
    }else{
        pid_t child2_id;
        child2_id = fork();
        while((wait(&status)) > 0);
        sleep(3);
        if(child2_id == 0){
            char *argv[] = {"mkdir", "-p", "home/dioz/modul2/air", NULL};
            execv("/bin/mkdir", argv);    
        }else{
            pid_t child3_id;
            int status1;
            child3_id = fork();
            if(child3_id == 0){
                char *argv[] = {"unzip", "-q", "-o", "animal.zip", "-d", "home/dioz/modul2", NULL};
                execv("/bin/unzip", argv);
            }else{
                while((wait(&status1)) > 0);
                pid_t child4_id;
                child4_id = fork();
                if(child4_id == 0){
                    char *argv[] = {"find", "home/dioz/modul2/animal", "-name", "*air*", "-exec", "mv", "-t", "home/dioz/modul2/air", "{}", "+", NULL};
                    execv("/bin/find", argv);
                }else{
                    pid_t child5_id;
                    int status2;
                    child5_id = fork();
                    if(child5_id == 0){
                        while((wait(&status2)) > 0);
                        pid_t child6_id;
                        int status3;
                        child6_id = fork();

                        if(child6_id == 0){
                            char *argv[] = {"find", "home/dioz/modul2/animal", "-name", "*darat*", "-exec", "mv", "-t", "home/dioz/modul2/darat", "{}", "+", NULL};
                            execv("/bin/find", argv);
                        }else{
                            while ((wait(&status3)) > 0);
                            pid_t child7_id;
                            child7_id = fork();
                            int status4;
                            if(child7_id == 0){
                                char *argv[] = {"find", "home/dioz/modul2/darat", "-name", "*bird*", "-exec", "rm", "{}", "+", NULL};
                                execv("/bin/find", argv);
                            }else{
                                while((wait(&status4)) > 0);
                                pid_t child8_id;
                                child8_id = fork();
                                int status5;
                                if(child8_id == 0){
                                    char *argv[] = {"touch", "home/dioz/modul2/air/list.txt", NULL};
                                    execv("/bin/touch", argv);
                                }else{
                                    while((wait(&status5)) > 0);
                                    listingFiles();
                                }
                            }
                        }
                    }else{
                        while((wait(&status2)) > 0);
                        char *argv[] = {"find", "home/dioz/modul2/animal", "-name", "*.jpg", "-exec", "rm", "{}", "+", NULL};
                        execv("/bin/find", argv);
                    }
                }
            }
        }
    }

    return 0;
}
